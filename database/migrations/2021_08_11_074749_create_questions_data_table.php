<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('table_id')->nullable();
            $table->text('votes')->nullable();
            $table->text('answers')->nullable();
            $table->text('views')->nullable();
            $table->mediumText('question_text')->nullable();
            $table->longText('question_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_data');
    }
}
