<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('head')
    <body>
        <div class="app-content mt-4">
            <center><a href="{{route('fetch.data')}}" style="width:50%" class="btn btn-md btn-success">SYNC DATA</a></center>
        </div>
        <div class="container mt-2">
            
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Total Number of Questions Per Tag</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="canvas">
                                    <canvas id="myChart" height="100"></canvas>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Total Number of Unanswered Questions Per Tag</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="canvas">
                                    <canvas id="unansweredChart" height="100" style="height:100px !important"></canvas>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Total Number of Bountied Questions Per Tag</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="canvas">
                                    <canvas id="bountiedChart" height="100"></canvas>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Total Number of Views Per Tag</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="canvas">
                                    <canvas id="viewsChart" height="100"></canvas>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    @include('script')
</html>
