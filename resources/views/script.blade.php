<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.0/dist/chart.min.js"></script>
<!-- BEGIN: Vendor JS-->
<script src="{{asset('app-assets/vendors/js/material-vendors.min.js')}}"></script>
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('app-assets/vendors/js/charts/chartist.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/charts/raphael-min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/charts/morris.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
<!-- END: Page Vendor JS-->
<!-- BEGIN: Theme JS-->
<script src="{{asset('app-assets/js/core/app-menu.js')}}"></script>
<script src="{{asset('app-assets/js/core/app.js')}}"></script>
<!-- END: Theme JS-->
<!-- BEGIN: Page JS-->
<script src="{{asset('app-assets/js/scripts/pages/material-app.js')}}"></script>
<!-- END: Page JS-->
<!-- BEGIN: Page JS-->
<script src="{{asset('app-assets/js/scripts/pages/date-picker.js')}}"></script>
<script type="text/javascript" src="{{asset('app-assets/js/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('app-assets/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
{{-- When u need introJS to work thats when u uncomment this --}}
{{-- @if(Session::has('intro_js'))
    <script> introJs().start(); </script>    
@endif --}}
{{-- @php Session::forget('intro_js') @endphp --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
<script>
   var drEvent = $('.dropify').dropify();
   drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
   });
</script>

<script>
    function getDisplayData() {
        var v = new Array();
        $.ajax({
            type: 'POST',
            url: '{{route('getNoOfTotalQuestions')}}',
            async: false,
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            success: function (response) {
                $(response).each(function (i, obj) {
                    v.push(response[i]);
                });
                console.log(v);
            }
        });
        return v;
    }

    function getUnansweredData() {
        var v = new Array();
        $.ajax({
            type: 'POST',
            url: '{{route('getNoOfTotalUnansweredQuestions')}}',
            async: false,
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            success: function (response) {
                $(response).each(function (i, obj) {
                    v.push(response[i]);
                });
                console.log(v);
            }
        });
        return v;
    }

    function getBountiedData() {
        var v = new Array();
        $.ajax({
            type: 'POST',
            url: '{{route('getNoOfTotalBountiedQuestions')}}',
            async: false,
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            success: function (response) {
                $(response).each(function (i, obj) {
                    v.push(response[i]);
                });
                console.log(v);
            }
        });
        return v;
    }
    
    function getViewsData() {
        var v = new Array();
        $.ajax({
            type: 'POST',
            url: '{{route('getNoOfTotalViews')}}',
            async: false,
            headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
            success: function (response) {
                $(response).each(function (i, obj) {
                    v.push(response[i]);
                });
                console.log(v);
            }
        });
        return v;
    }

    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['PHP', 'Laravel', 'NodeJS', 'Express', 'Ruby', 'GoLang', 'Go', 'C', 'HTML'],
            datasets: [{
                label: '# of Questions',
                data: getDisplayData(),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(144, 12, 15, 0.2)',
                    'rgba(122, 18, 8, 0.2)',
                    'rgba(90, 58, 118, 0.2)',
                    'rgba(116, 212, 136, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(144, 12, 15, 1)',
                    'rgba(122, 18, 8, 1)',
                    'rgba(90, 58, 118, 1)',
                    'rgba(116, 212, 136, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var unansweredChart = document.getElementById('unansweredChart');
    var uChart = new Chart(unansweredChart, {
        type: 'line',
        data: {
            labels: ['PHP', 'Laravel', 'NodeJS', 'Express', 'Ruby', 'GoLang', 'Go', 'C', 'HTML'],
            datasets: [{
                label: '# of Questions',
                data: getUnansweredData(),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(144, 12, 15, 0.2)',
                    'rgba(122, 18, 8, 0.2)',
                    'rgba(90, 58, 118, 0.2)',
                    'rgba(116, 212, 136, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(144, 12, 15, 1)',
                    'rgba(122, 18, 8, 1)',
                    'rgba(90, 58, 118, 1)',
                    'rgba(116, 212, 136, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var bountiedChart = document.getElementById('bountiedChart');
    var bChart = new Chart(bountiedChart, {
        type: 'bar',
        data: {
            labels: ['PHP', 'Laravel', 'NodeJS', 'Express', 'Ruby', 'GoLang', 'Go', 'C', 'HTML'],
            datasets: [{
                label: '# of Questions',
                data: getBountiedData(),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(144, 12, 15, 0.2)',
                    'rgba(122, 18, 8, 0.2)',
                    'rgba(90, 58, 118, 0.2)',
                    'rgba(116, 212, 136, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(144, 12, 15, 1)',
                    'rgba(122, 18, 8, 1)',
                    'rgba(90, 58, 118, 1)',
                    'rgba(116, 212, 136, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var viewChart = document.getElementById('viewsChart');
    var vChart = new Chart(viewChart, {
        type: 'bar',
        data: {
            labels: ['PHP', 'Laravel', 'NodeJS', 'Express', 'Ruby', 'GoLang', 'Go', 'C', 'HTML'],
            datasets: [{
                label: '# of Views',
                data: getViewsData(),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(144, 12, 15, 0.2)',
                    'rgba(122, 18, 8, 0.2)',
                    'rgba(90, 58, 118, 0.2)',
                    'rgba(116, 212, 136, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(144, 12, 15, 1)',
                    'rgba(122, 18, 8, 1)',
                    'rgba(90, 58, 118, 1)',
                    'rgba(116, 212, 136, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>