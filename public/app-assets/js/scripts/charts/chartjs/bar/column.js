/*=========================================================================================
    File Name: column.js
    Description: Chartjs column chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Column chart
// ------------------------------
$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#column-chart");

    var topctx = $("#top-chart");

    var courierctx = $("#courier-chart");
    // Chart Options
    var chartOptions = {
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each bar to be 2px wide and green
        elements: {
            rectangle: {
                borderWidth: 2,
                borderColor: 'rgb(0, 255, 0)',
                borderSkipped: 'bottom'
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        plugins: {
            labels: {
                render: 'value',
                fontColor: '#666',
                fontSize: 14,
                fontStyle: 'bold',
                fontFamily: 'Muli, sans-serif',
                position: 'outside',
                outsidePadding: 0,
                textMargin: 0
            }
        },
        legend: {
            display:false,
            position: 'top',
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display:true
                },
                maxBarThickness: 50,
            }],
            yAxes: [{
                gridLines: {
                    display:true
                },
                ticks: {
                    beginAtZero: true
                } 
            }]
        }
    };

    // Chart Data
    var chartData = {
        labels: ["Delhi", "Karnataka", "Rajasthan", "Mumbai", "Kolkata"],
        datasets: [{
            data: [65, 59, 810, 81, 56],
            backgroundColor: "#FF9800",
            hoverBackgroundColor: "rgba(22,211,154,.9)",
            borderColor: "transparent"
        }]
    };


    var topchartData = {
        labels: ["Delhi", "Karnataka", "Rajasthan", "Mumbai", "Kolkata"],
        datasets: [{
            data: [65, 59, 810, 81, 56],
            backgroundColor: "#387b41",
            hoverBackgroundColor: "rgba(22,211,154,.9)",
            borderColor: "transparent"
        }]
    };

    var courierData = {
        labels: ["Delhivery", "Xpressbees", "Ecom Express"],
        datasets: [{
            data: [7, 10, 23],
            backgroundColor: ["#36f4d1","#36f48b","#d7f436"],
            hoverBackgroundColor: "rgba(22,211,154,.9)",
            borderColor: "transparent"
        }]
    };


    var config = {
        type: 'bar',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    var topconfig = {
        type: 'bar',

        // Chart Options
        options : chartOptions,

        data : topchartData
    };

    var courierconfig = {
        type: 'bar',

        // Chart Options
        options : chartOptions,

        data : courierData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);

    var toplineChart = new Chart(topctx, topconfig);

    var courierlineChart = new Chart(courierctx, courierconfig);


});