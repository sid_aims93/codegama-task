/*=========================================================================================
    File Name: line.js
    Description: Chartjs simple line chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Line chart
// ------------------------------
$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#line-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        plugins: {
            labels: {
                render: function (args) {
                    return args.value +'%';
                },
                fontColor: '#666',
                fontSize: 14,
                fontStyle: 'bold',
                fontFamily: 'Muli, sans-serif',
                position: 'outside',
                outsidePadding: 0,
                textMargin: 0
            }
        },
        legend: {
            display:false,
            position: 'top',
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display:true
                }
            }],
            yAxes: [{
                gridLines: {
                    display:true
                }
            }]
        }
    };

    // Chart Data
    var chartData = {
        labels: [1,2,3,4,5,6,7],
        datasets: [{
            data: [24500, 121301, 4500, 212000, 12345, 23233, 7600],
            lineTension: 0,
            fill: false,
            borderColor: "#387b41",
            pointBorderColor: "#387b41",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };

    var config = {
        type: 'line',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);
});