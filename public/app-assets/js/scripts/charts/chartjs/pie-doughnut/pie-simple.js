/*=========================================================================================
    File Name: pie.js
    Description: Chartjs pie chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Pie chart
// ------------------------------
$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#simple-pie-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        plugins: {
            labels: {
                render: function (args) {
                    return args.value +'%';
                },
                fontColor: ['#fff', '#fff'],
                fontSize: 14,
                fontStyle: 'bold',
                fontFamily: 'Muli, sans-serif'
            }
        },
        legend: {
            position:'top',
            align:'start'
        }      
    };

    // Chart Data
    var chartData = {
        labels: ["Prepaid", "COD"],
        datasets: [{
            label: "Prepaid vs COD",
            data: [28, 72],
            backgroundColor: ['#387b41', '#656565'],
        }]
    };

    var config = {
        type: 'pie',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var pieSimpleChart = new Chart(ctx, config);
});