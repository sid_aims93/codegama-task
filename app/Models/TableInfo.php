<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableInfo extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getTableData() {
        return $this->hasMany(TableData::class, 'tag_id');
    }
}
