<?php

namespace App\Http\Controllers;

use App\Models\QuestionsData;
use App\Models\TableData;
use App\Models\TableInfo;
use Exception;
use Illuminate\Http\Request;
use Goutte\Client;


/** Every thing is written here in this file split into functions so the tracking is easy */
class HomeController extends Controller {

    private $tags, $appends, $questions_text, $questions_data;

    public function __construct() {
        //The 10 tags as mentioned in the statement
        $this->tags = ['php', 'laravel', 'nodejs', 'express', 'expressjs', 'ruby', 'golang', 'go', 'c', 'html'];
        $this->appends = ['Newest', 'Active', 'Bounties', 'Unanswered'];   
    }

    public function index() {
        return view('index');
    }

    public function getNoOfTotalUnansweredQuestions() {
        try {
            $return_arr = [];
            $rows = TableInfo::all();
            foreach($rows as $row)
                $return_arr[] = $row->getTableData()->where('type', 'Unanswered')->sum('questions');
            return $return_arr;
        } catch(Exception $e) {
            return [];
        }
    }

    public function getNoOfTotalViews() {
        try {
            $return_arr = [];
            $rows = TableInfo::all();
            foreach($rows as $row) {
                $temp = $row->getTableData()->select('id')->get()->pluck('id')->toArray();
                $return_arr[] = QuestionsData::whereIn('table_id', $temp)->sum('views');
            }
            return $return_arr;
        } catch(Exception $e) {
            return [];
        }
    }
 
    public function getNoOfTotalBountiedQuestions() {
        try {
            $return_arr = [];
            $rows = TableInfo::all();
            foreach($rows as $row)
                $return_arr[] = $row->getTableData()->where('type', 'Bounties')->sum('questions');
            return $return_arr;
        } catch(Exception $e) {
            return [];
        }
    }

    public function getNoOfTotalQuestions() {
        try {
            $return_arr = [];
            $rows = TableInfo::all();
            foreach($rows as $row)
                $return_arr[] = $row->getTableData()->sum('questions');
            return $return_arr;
        } catch(Exception $e) {
            return [];
        }
    }

    private function saveData($tag, $data) {
        $row = TableInfo::create([
            'language' => $tag
        ]);
        if(isset($data[$tag]) && is_array($data[$tag]) && count($data[$tag]) > 0) 
            foreach($data[$tag] as $type => $analytics_data) {
                $table_row = TableData::create([
                    'tag_id' => $row->id,
                    'type' => $type,
                    'questions' => $analytics_data['total_questions']
                ]);
                if(isset($analytics_data['questions_data']) && is_array($analytics_data['questions_data']) && count($analytics_data['questions_data']) > 0)
                    foreach($analytics_data['questions_data'] as $q_data) 
                        QuestionsData::create([
                            'table_id' => $table_row->id,
                            'votes' => $q_data['votes'],
                            'answers' => str_replace('s', '', $q_data['answers']),
                            'views' => str_replace('k', '000', $q_data['views']),
                            'question_text' => $q_data['question'],
                            'question_link' => $q_data['link']
                        ]);
            }
    }

    public function getData() {
        try {
            TableInfo::truncate();
            TableData::truncate();
            QuestionsData::truncate();
            foreach($this->tags as $tag) {
                $data = [];
                foreach($this->appends as $get_param) {
                    $client = new Client();
                    $crawler = $client->request('GET', 'https://stackoverflow.com/questions/tagged/'.$tag.'?tab='.$get_param);
                    $data[$tag][$get_param] = $this->extractDataFromThisClient($crawler);             
                }
                $this->saveData($tag, $data);
            }
            return redirect()->route('display.data');
        } catch(Exception $e) {
            dd($e->getMessage().' '.$e->getLine());
        }
    }

    private function extractDataFromThisClient($crawler) {
        return [
            'total_questions' => $this->getTotalQuestions($crawler),
            'questions_data' => $this->getQuestionsForThisPage($crawler)
        ];
    }

    private function getTotalQuestions($crawler) {
        try {
            $this->questions_text = null;
            $crawler->filter('.fs-body3')->each(function ($node) {
                $this->questions_text = str_replace('questions', '' , $node->text());
                $this->questions_text = str_replace(',', '', $this->questions_text);
            });
            return intval($this->questions_text) ?? 0;
        } catch(Exception $e) {
            return 0;
        }
    }

    private function getQuestionsForThisPage($crawler) {
        try {
            $this->questions_data = [];
            $crawler->filter('.mln24')->each(function ($node) {
                $this->questions_data[] = $this->getDataForThisNode($node);
            });
            return $this->questions_data;
        } catch(Exception $e) {
            return [];
        }
    }

    private function getDataForThisNode($node) {    
        try {
            return [
                'votes' => $node->filter('.vote-count-post')->text(),
                'answers' => str_replace(['answer', 'answers'], '', $node->filter('.status')->text()),
                'views' => str_replace(' views', '', $node->filter('.views')->text()),
                'question' => $node->filter('.question-hyperlink')->text(),
                'link' => $node->filter('.question-hyperlink')->link()->getUri()
            ];
        } catch(Exception $e) {
            throw $e;
        }
    }
}
