<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

//Web routes
Route::get('/', [HomeController::class, 'index'])->name('display.data');
Route::get('getData', [HomeController::class, 'getData'])->name('fetch.data');

//Data fetching routes
Route::post('getNoOfTotalQuestions', [HomeController::class, 'getNoOfTotalQuestions'])->name('getNoOfTotalQuestions');
Route::post('getNoOfTotalUnansweredQuestions', [HomeController::class, 'getNoOfTotalUnansweredQuestions'])->name('getNoOfTotalUnansweredQuestions');
Route::post('getNoOfTotalBountiedQuestions', [HomeController::class, 'getNoOfTotalBountiedQuestions'])->name('getNoOfTotalBountiedQuestions');
Route::post('getNoOfTotalViews', [HomeController::class, 'getNoOfTotalViews'])->name('getNoOfTotalViews');